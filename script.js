;(function(window, document, undefined){

	const config = {
		apiUri: 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=56.84,55.27,33.48,41.48',
		baseCoords: {
			lat: 55.410307,
			long: 37.902451
		},
		rawDataMap: {
			coordLat: 1,
			coordLong: 2,
			speedKts: 5,
			track: 3,
			altitudeFt: 4,
			airportFrom: 11,
			airportTo: 12,
			flightNum: 13,
		}
	};

	const appNode = document.querySelector('.app')
	const loadingNode = document.querySelector('.app__loading');

	async function tick() {
		let data
		loadingNode.classList.add('app__loading_active');
		try {
			data = await fetch(config.apiUri).then(res => res.json());
			setTimeout(tick, 3000);
		} catch(e) {
			appNode.classList.add('app_error');
		} finally {
			loadingNode.classList.remove('app__loading_active');
		}
		data && render(data);
	}

	function render(data) {

		// Чтобы определить, если структура данных от api вдруг изменилась, будем проверять version
		if (data.version !== 4) throw '[WARNING] Version of api has changed! Version 4 is expected.';

		// Извлекаем только данные о рейсах
		delete data.version;
		delete data.full_count;
		const flightsRaw = Object.keys(data).map(key => data[key]);

		// Формируем необходимые для отображения данные
		let flightsHandled = flightsRaw.map(flight => {
			const dataMap = config.rawDataMap;
			const coordLat = flight[dataMap['coordLat']]
			const coordLong = flight[dataMap['coordLong']]

			// Координаты через ","
			const coords = `${coordLat}, ${coordLong}`;

			// Скорость переводим из узлов в км/ч
			const speed = Math.floor(parseInt(flight[dataMap['speedKts']]) * 1.852);

			// Курс
			const track = flight[dataMap['track']];

			// Высоту переводим из футов в м
			const altitude = Math.floor(flight[dataMap['altitudeFt']] * 0.3048);

			// Пункт отправления
			const airporFrom = flight[dataMap['airportFrom']];

			// Пункт назначения
			const airporTo = flight[dataMap['airportTo']];

			// Номер рейса
			const flightNum = flight[dataMap['flightNum']];

			// Расстояния до аэропорта
			const distance = Math.floor(getDistance(coordLat, coordLong, config.baseCoords.lat, config.baseCoords.long));

			return {data: [coords, speed, track, altitude, airporFrom, airporTo, flightNum], distance};
		});
		
		// Сортируем по расстоянию
		flightsHandled = flightsHandled.sort((a, b) => a.distance - b.distance);

		// Cобираем поддерево DOM
		const listNode = document.querySelector('.app__list')
		const newListNode = listNode.cloneNode();
		flightsHandled.forEach(flight => {
			const rowNode = document.querySelector('.app__template').content.cloneNode(true);
			rowNode.querySelectorAll('.app__data').forEach((cell, i) => {
				cell.innerHTML = flight.data[i] !== '' ? flight.data[i] : '—';
			});
			newListNode.append(rowNode);
		});

		// Подменяем старый список на новый
		listNode.replaceWith(newListNode);
	}

	function getDistance(lat1, lon1, lat2, lon2) {
		const radlat1 = Math.PI * lat1/180
		const radlat2 = Math.PI * lat2/180
		const theta = lon1-lon2
		const radtheta = Math.PI * theta/180
		let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist)
		dist = dist * 180/Math.PI
		dist = dist * 60 * 1.1515
		dist = dist * 1.609344
		return dist
	}

	tick();

})(window, document);